package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.print("Let's play round " + roundCounter + "\nYour choice (Rock/Paper/Scissors)?");  
			String human_choice = sc.next();
		
			
			Random number = new Random();
			int random = number.nextInt(rpsChoices.size());
			
			
	        String random_choice = rpsChoices.get(random);
	        
	        if (random_choice.equals(human_choice)) {
	        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". It´s a tie");
	        	System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
	        }	else if (random_choice.equals(rpsChoices.get(0)) && human_choice.equals(rpsChoices.get(1))) {
		        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". Human wins!");
		        	humanScore += 1;
		    		System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
	        	
	        }	else if (random_choice.equals(rpsChoices.get(2)) && human_choice.equals(rpsChoices.get(0))) {
		        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". Human wins!");
		        	humanScore += 1;     
		    		System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
		        	
	        }	else if (random_choice.equals(rpsChoices.get(1)) && human_choice.equals(rpsChoices.get(2))) {
		        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". Human wins!");
		        	humanScore += 1;
		    		System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
	        }	
	        	else if (random_choice.equals(rpsChoices.get(0)) && human_choice.equals(rpsChoices.get(2))) {
		        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". Computer wins!");
		            computerScore += 1;
		    		System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
	        	
	        }	else if (random_choice.equals(rpsChoices.get(1)) && human_choice.equals(rpsChoices.get(0))) {
		        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". Computer wins!");
		        	computerScore += 1;
		    		System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
	        			
	        }	else if (random_choice.equals(rpsChoices.get(2)) && human_choice.equals(rpsChoices.get(1))) {
		        	System.out.print("\nHuman chose " + human_choice + ", computer chose " + random_choice + ". Computer wins!");
		        	computerScore += 1;
		    		System.out.printf("\nScore: human " + humanScore + ", computer " + computerScore);
	    		
	
	        }
	        	else { 
	        		System.out.print("I do not understand " + human_choice + ". Could you try again?\n");
	        		continue;
	        	}
	        
			
			roundCounter += 1;
			
			System.out.print("\nDo you wish to continue playing? (y/n)?");
			String continue_choice = sc.next();
			
			if (continue_choice.equals("y")) {
				continue;
			}	else {
					System.out.print("\nBye bye :)");
					break;
			}
	}
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
